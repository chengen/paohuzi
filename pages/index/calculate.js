// pages/index/calculate.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    userList: [{
        user: 'A',
        name: "甲",
      },
      {
        user: 'B',
        name: '乙',
      },
      {
        user: 'C',
        name: '丙',
      },
      {
        user: 'D',
        name: '丁',
      }
    ]
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  },

  formBindsubmit: function(e) {
    var valueA = e.detail.value.A;
    var valueB = e.detail.value.B;
    var valueC = e.detail.value.C;
    var valueD = e.detail.value.D;

    var size = e.detail.value.multiplier;
    var valueABird = e.detail.value.BirdForA * 1;
    var valueBBird = e.detail.value.BirdForB * 1;
    var valueCbird = e.detail.value.BirdForC * 1;
    var valueDbird = e.detail.value.BirdForD * 1;

    var TuoA = e.detail.value.ATuo;
    var TuoB = e.detail.value.BTuo;
    var TuoC = e.detail.value.CTuo;
    var TuoD = e.detail.value.DTuo;

    let result = this.calculateFinal(valueA, valueB, valueC, valueD, size,
      valueABird, valueBBird, valueCbird, valueDbird, TuoA, TuoB, TuoC, TuoD)

    this.setData({
      result: result,
    })
  },




  calculateFinal(A, B, C, D, Multiplier,
    BirdA, BirdB, BirdC, BirdD,
    StatusA, StatusB, StatusC, StatusD) {

    let input = [{
        score: A,
        niao: BirdA,
        tuo: StatusA,
      },
      {
        score: B,
        niao: BirdB,
        tuo: StatusB,
      },
      {
        score: C,
        niao: BirdC,
        tuo: StatusC,
      },
      {
        score: D,
        niao: BirdD,
        tuo: StatusD,
      }
    ];

    input.forEach(p => {
      p._score = p.score;
      p.score = this.round(p._score);
    });

    for (var i = 0; i < input.length; i++) {
      let result = 0;
      let a = input[i];
      for (var j = 0; j < input.length; j++) {
        if (i != j) {
          let times = 1;
          let b = input[j];
          if (a.tuo && b.tuo) times = 3;
          else if (a.tuo || b.tuo) times = 2;
          let r = (a.score - b.score) * times;

          if (a.score > b.score) r += (a.niao + b.niao)
          else if (a.score < b.score) r -= (a.niao + b.niao)
          result += r;
        }
      }
      a.result = result * Multiplier;

    }
    console.log('input', input)
    return input.map(p => p.result);
  },

  round(n) {
    n = parseInt(n);
    let k = (n + 0.1) / 10 ;
    return Math.round(k) * 10;
  }
});